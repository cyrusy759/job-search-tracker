from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from routers import accounts
from authenticator import authenticator

app = FastAPI()

app.include_router(authenticator.router, tags=["authenticator"])
app.include_router(accounts.router, tags=["accounts"])

origins = [
    "http://localhost:3000",
    os.environ.get("REACT_APP_FASTAPI_SERVICE_API_HOST", None),
    os.environ.get("CORS_HOST", None),
    os.environ.get("PUBLIC_URL", None),
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)



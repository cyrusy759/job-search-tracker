from pydantic import BaseModel
from typing import Literal
from bson.objectid import ObjectId

class PydanticObjectId(ObjectId):
    @classmethod
    def get_validators(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value: ObjectId | str) -> ObjectId:
        if value:
            try:
                ObjectId(value)
            except ValueError:
                raise ValueError(f"Not a valid object id: {value}")
        return value


class Account(BaseModel):
    id: PydanticObjectId
    username: str
    password: str
    email: str
    first_name: str
    last_name: str
    avatar: str | None
    role: Literal['searcher', 'hirer']


class AccountIn(BaseModel):
    username: str
    password: str
    email: str
    first_name: str
    last_name: str


class AccountOut(BaseModel):
    id: str
    username: str
    password: str
    email: str
    first_name: str
    last_name: str
    avatar: str | None
    role: Literal['searcher', 'hirer']


class AccountUpdateForm(BaseModel):
    username: str | None
    first_name: str | None
    last_name: str | None
    avatar: str | None

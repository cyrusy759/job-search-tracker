from pydantic import BaseModel

class SearcherProfile(BaseModel):
    account_id: str
    account_email: str
    id: str
    first_name: str
    last_name: str
    resume: str
    cover_letter: str


class SearcherProfileIn(BaseModel):
    first_name: str
    last_name: str
    resume: str
    cover_letter: str
    

class SearcherProfileOut(BaseModel):
    account_email: str
    first_name: str
    last_name: str
    resume: str
    cover_letter: str


class SearcherProfileUpdateForm(BaseModel):
    first_name: str
    last_name: str
    resume: str
    cover_letter: str

    


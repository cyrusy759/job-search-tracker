from pydantic import BaseModel

class SearcherProfile(BaseModel):
    account_id: str
    account_email: str
    id: str
    first_name: str
    last_name: str
    jobs_applied: int
    interviews: int
    offers: int


class SearcherProfileIn(BaseModel):
    first_name: str
    last_name: str
    jobs_applied: int
    interviews: int
    offers: int
    

class SearcherProfileOut(BaseModel):
    account_email: str
    first_name: str
    last_name: str
    jobs_applied: int
    interviews: int
    offers: int


class SearcherProfileUpdateForm(BaseModel):
    first_name: str
    last_name: str
    jobs_applied: int
    interviews: int
    offers: int
    


from .client import Queries
from ..models.searcher import (
    SearcherProfile,
    SearcherProfileIn,
    SearcherProfileUpdateForm,
    SearcherProfileOut,
)


class DuplicateError(ValueError):
    pass


class NoProfileError(ValueError):
    pass


class SearcherQueries(Queries):
    DB_NAME = "searching"
    COLLECTION = "jobs"

    def get(self, account_id: str) -> SearcherProfile:
        props = self.collection.find_one({"account_id": account_id})
        if not props:
            return {"message": "Trainee profile does not exist"}
        props["id"] = str(props["_id"])
        return SearcherProfile(**props)

    def create(self, info: SearcherProfileIn, account_data) -> SearcherProfile:
        props = info.dict()
        props["account_id"] = account_data["id"]
        props["account_email"] = account_data["email"]
        if self.collection.find_one({"account_id": props["account_id"]}):
            raise DuplicateError()
        if self.collection.find_one({"account_email": props["account_email"]}):
            raise DuplicateError()

        self.collection.insert_one(props)

        props["id"] = str(props["_id"])

        return SearcherProfile(**props)

    def update(
        self, info: SearcherProfileUpdateForm, account_email: str
    ) -> SearcherProfileOut:
        props = self.collection.find_one({"account_email": account_email})
        try:
            props["id"] = str(props["_id"])
        except TypeError:
            raise NoProfileError()
        for k, v in info.dict().items():
            if v is None or v == "":
                pass
            else:
                props[k] = v

        self.collection.update_one(
            {"account_email": account_email},
            {
                "$set": {
                    "first_name": props["first_name"],
                    "last_name": props["last_name"],
                    "jobs_applied": props["jobs_applied"],
                    "interviews": props["interviews"],
                    "offers": props["offers"],
                }
            },
        )

        return SearcherProfileOut(**props)

    def delete(self, account_email) -> dict:
        status = self.collection.delete_one({"account_email": account_email})
        if status.deleted_count:
            return {"message": "profile deleted successfully"}
        else:
            return {"message": "profile deletion failed"}

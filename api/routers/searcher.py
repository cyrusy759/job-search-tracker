from fastapi import (
    Depends,
    HTTPException,
    status,
    APIRouter,
)

from authenticator import authenticator
from queries.searcher import (
    SearcherQueries,
    DuplicateError,
    NoProfileError,
)
from ..models.searcher import (
    SearcherProfileIn,
    SearcherProfileOut,
    SearcherProfileUpdateForm,
)
from typing import Optional
from pydantic import BaseModel

router = APIRouter()


class HttpError(BaseModel):
    detail: str


@router.post(
    "/api/searcher_profiles",
    response_model=SearcherProfileOut | HttpError | dict,
)
async def create_searcher_profile(
    info: SearcherProfileIn,
    repo: SearcherQueries = Depends(),
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
):
    if account_data:
        try:
            searcher = repo.create(info, account_data)
            return searcher
        except DuplicateError:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Already exists.",
            )
    raise HTTPException(
        status_code=status.HTTP_400_BAD_REQUEST,
        detail="Profile cannot be created.",
    )


# Route to be determined once front-end is more defined
@router.get(
    "/api/searcher_profiles",
    response_model=SearcherProfileOut | dict,
)
async def get_searcher_profile(
    repo: SearcherQueries = Depends(),
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
):
    if account_data:
        searcher = repo.get(account_data["id"])
        return searcher
    return {"message": "profile cannot be retrieved if not logged in"}


@router.put(
    "/api/searcher_profiles",
    response_model=SearcherProfileOut | HttpError | dict,
)
async def update_searcher_profile(
    info: SearcherProfileUpdateForm,
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
    repo: SearcherQueries = Depends(),
):
    if account_data:
        account_email = account_data["email"]
        try:
            searcher_profile = repo.update(info, account_email)
            return searcher_profile
        except NoProfileError:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="You need to create a profile.",
            )
    return {"message": "no account logged in"}


@router.delete("/api/searcher_profiles", response_model=dict)
async def delete_searcher_profile(
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
    repo: SearcherQueries = Depends(),
):
    if account_data:
        account_email = account_data["email"]
        message = repo.delete(account_email)
        return message
    return {"message": "no account logged in"}

import React, { useState } from 'react';
import './dnd.css';

const initialJsonData = {
  name: 'John Doe',
  age: 30,
  email: 'johndoe@example.com',
};

const DragAndDropJson = () => {
  const [jsonData, setJsonData] = useState(initialJsonData);
  const [isDragging, setIsDragging] = useState(false);

  const handleDragStart = (event) => {
    event.dataTransfer.setData('application/json', JSON.stringify(jsonData));
    setIsDragging(true);
  };

  const handleDragEnd = () => {
    setIsDragging(false);
  };

  const handleDragOver = (event) => {
    event.preventDefault();
  };

  const handleDrop = (event) => {
    event.preventDefault();
    setIsDragging(false);
    const droppedData = JSON.parse(event.dataTransfer.getData('application/json'));
    setJsonData(droppedData);
  };

  return (
    <div
      className={`drag-and-drop ${isDragging ? 'dragging' : ''}`}
      onDragStart={handleDragStart}
      onDragEnd={handleDragEnd}
      onDragOver={handleDragOver}
      onDrop={handleDrop}
      draggable
    >
      <pre>{JSON.stringify(jsonData, null, 2)}</pre>
    </div>
  );
};

export default DragAndDropJson;
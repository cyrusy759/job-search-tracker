import React from 'react';
import DragAndDrop from './dnd.jsx';
import JobsBox from './jobs_box';
import InterviewsBox from './interviews_box.jsx';
import OffersBox from './offers_box.jsx';

function DND2App() {
  return (
    <div>
      <h1>Drag and Drop Example</h1>
      <DragAndDrop />
      <JobsBox />
      <InterviewsBox />
      <OffersBox />
    </div>
  );
}

export default DND2App;
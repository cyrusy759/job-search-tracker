import React from 'react';
import './box.css';

const OffersBox = () => {
  const handleDragOver = (event) => {
    event.preventDefault();
  };

  const handleDrop = (event) => {
    event.preventDefault();
    const data = event.dataTransfer.getData('application/json');
    console.log('Dropped:', data);
  };

  return (
    <div className="drag-into-box" onDragOver={handleDragOver} onDrop={handleDrop}>
      Offers
    </div>
  );
};

export default OffersBox;
import React, { useState } from 'react';
import axios from 'axios';
import { PDFExtract } from 'pdf.js-extract';

const ResumeScanner = () => {
  const [selectedFile, setSelectedFile] = useState(null);
  const [uploadMessage, setUploadMessage] = useState('');

  const handleFileChange = (event) => {
    setSelectedFile(event.target.files[0]);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (!selectedFile) {
      setUploadMessage('Please select a file before uploading.');
      return;
    }

    const formData = new FormData();
    formData.append('resume', selectedFile);

    try {
      await axios.post('/api/scan_resume/', formData);
      setUploadMessage('Resume uploaded and scanned successfully.');
    } catch (error) {
      console.error('Error uploading resume:', error);
      setUploadMessage('Error uploading resume. Please try again.');
    }
  };

  return (
    <div>
      <h1>Resume Scanner</h1>
      <form onSubmit={handleSubmit}>
        <input type="file" accept=".pdf,.doc,.docx" onChange={handleFileChange} />
        <button type="submit">Upload Resume</button>
      </form>
      <p>{uploadMessage}</p>
    </div>
  );
};

export default ResumeScanner;
import { useState } from "react";
import axios from "axios";

function LoginForm() {
    const [credentials, setCredentials] = useState({
        username: '',
        password: '',
    })
    const handleFormChange = (event) => {
        setCredentials({...credentials, [event.target.name]: event.target.value});
    }
    const handleSubmit = async(event) => {
        event.preventDefault();
        console.log(credentials)
        axios.post(`${process.env.REACT_APP_API_HOST}/token`, credentials)
    }

    return(
        <div>
            <label>username</label>
            <input type='text'
                onChange={handleFormChange} 
                name='username' 
                value={credentials.username}></input>
            <br/>
            <label>password</label>
            <input type='text'
                onChange={handleFormChange} 
                name='password' 
                value={credentials.password}></input>
            <br/>
            <button onClick={handleSubmit}>submit</button>
        </div>
    );
}

export default LoginForm;
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const blahApi = createApi({
	reducerPath: "logBlah",
	baseQuery: fetchBaseQuery({
		baseUrl: process.env.REACT_APP_API_HOST,
	}),
	tagTypes: ["Blah"],
	endpoints: (builder) => ({
		getBlah: builder.query({
			query: () => {
				return {
					url: "/api/blah",
					credentials: "include",
				};
			},
			providesTags: ["Blah"],
		}),
		createExercise: builder.mutation({
			query: (info) => {
				return {
					url: "/api/blah",
					method: "post",
					body: info,
					credentials: "include",
				};
			},
			invalidatesTags: ["Blah"],
			async onQueryStarted(arg, { queryFulfilled }) {
				try {
					await queryFulfilled;
				} catch (err) {
					return err;
				}
			},
		}),
		deleteExercise: builder.mutation({
			query: (blahId) => {
				return {
					url: `/api/exercises/${blahId}`,
					method: "delete",
					credentials: "include",
				};
			},
			invalidatesTags: ["Blah"],
			async onQueryStarted(arg, { queryFulfilled }) {
				try {
					await queryFulfilled;
					return { message: "excercise successfully deleted" };
				} catch (err) {
					return err;
				}
			},
		}),
	}),
});

export const {
	useGetExercisesQuery,
	useCreateExerciseMutation,
	useDeleteExerciseMutation,
} = exerciseApiSlice;
